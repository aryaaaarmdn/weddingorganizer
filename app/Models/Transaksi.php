<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Transaksi extends Model
{
    use HasFactory;
    protected $table = 'transaksi';

    // Relasi table transaksi - user . Many to many
    public function users()
    {
        return $this->belongsToMany(User::class);
    }

    // Relasi table transaksi - jasa . Many to many
    public function jasa()
    {
        return $this->belongsToMany(Jasa::class);
    }

    // Relasi table transaksi - jenis_pembayaran. Many to many (inverse)
    public function jenisPembayaran()
    {
        return $this->belongsTo(JenisPembayaran::class);
    }

    // Relasi table transaksi - status_pembayaran. Zero/One to One (Inverse)
    public function statusPembayaran()
    {
        return $this->belongsTo(StatusPembayaran::class);
    }

}
