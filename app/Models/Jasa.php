<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Jasa extends Model
{
    use HasFactory;
    protected $table = 'jasa';

    // Relasi table jasa - transaksi. Many to Many
    public function transaksi()
    {
        return $this->belongsToMany(Transaksi::class);
    }
}
