<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class JenisPembayaran extends Model
{
    use HasFactory;
    protected $table = 'jenis_pembayaran';

    // Relasi table jenis_pembayaran - transaksi. Many to many
    public function transaksi()
    {
        return $this->hasMany(Transaksi::class);
    }
}
