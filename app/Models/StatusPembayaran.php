<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class StatusPembayaran extends Model
{
    use HasFactory;
    protected $table = 'status_pembayaran';

    // Relasi table status_pembayaran - transaksi. Zero/One to One
    public function transaksi()
    {
        return $this->hasOne(Transaksi::class);
    }
}
