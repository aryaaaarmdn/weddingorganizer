<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\LoginRequest;
use App\Providers\RouteServiceProvider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthenticatedSessionController extends Controller
{
    /**
     * Display the login view.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('auth.login');
    }

    /**
     * Handle an incoming authentication request.
     *
     * @param  \App\Http\Requests\Auth\LoginRequest  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    
    public function store(Request $request)
    {

        if (
            Auth::attempt($request->only('email', 'password'), $request->boolean('remember'))
        ) {

            $request->session()->regenerate();

            if ( Auth::user()->is_admin == 1 ) {
                return redirect()->intended(route('admin.dashboard'));
            } else {
                return redirect()->intended(route('dashboard'));
            }

        }
        return redirect()->back()->with('gagal_login', 'Username / Password Salah');
    }

    /**
     * Destroy an authenticated session.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Request $request)
    {
        Auth::guard('web')->logout();

        $request->session()->invalidate(); // Hapus semua data session

        // Ganti dengan ini untuk generate ulang id session (Ada pada dokumentasi)
        // $request->session()->regenerate();

        $request->session()->regenerateToken();

        return redirect('/');
    }
}
