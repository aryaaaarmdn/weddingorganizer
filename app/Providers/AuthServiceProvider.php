<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;
use App\Models\User;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Models\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        // AdminLTE Menu (Buyer)
        Gate::define('buyer-menu', function (User $user) {
            return $user->is_admin === 0;
        });

        // AdminLTE Menu (Admin)
        Gate::define('admin-menu', function (User $user) {
            return $user->is_admin === 1;
        });
    }
}
