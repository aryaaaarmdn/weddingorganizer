<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\PembeliController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// Hanya untuk logout/testing (Akan dihapus dikemudian waktu)
Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth'])->name('dashboard');

// Account Routing
Route::group(
    [
        'prefix' => 'account',
        'middleware' => ['auth', 'is_buyer', 'is_admin']
    ], function() {
        Route::get('/profile', function() {
            return 'Profile Route';
        })->name('profile');

        Route::get('/ubah-password', function() {
            return 'Ubah Password Route';
        })->name('ubahPassword.view');
    }
);

// Buyer Routing
Route::group(
    [
        'prefix' => 'user',
        'middleware' => 'is_buyer',
    ], function() {
        Route::get('/dashboard', [PembeliController::class, 'index'])
            ->name('dashboard');
    }
);

// Admin Routing
Route::group(
    [
        'prefix' => 'admin',
        'middleware' => 'is_admin'
    ], function() {
        Route::get('/dashboard', [AdminController::class, 'index'])
            ->name('admin.dashboard');
    }
);

require __DIR__.'/auth.php';

