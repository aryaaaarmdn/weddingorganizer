<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateJasaTransaksiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jasa_transaksi', function (Blueprint $table) {
            $table->id();
            $table->foreignId('jasa_id')
                    ->constrained('jasa')
                    ->onUpdate('cascade')
                    ->onDelete('cascade');
            $table->foreignUuid('transaksi_id')
                    ->constrained('transaksi')
                    ->onUpdate('cascade')
                    ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jasa_transaksi');
    }
}
