<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransaksisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaksi', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->foreignId('user_id')->constrained();
            $table->integer('total_harga');
            $table->foreignId('status_pembayaran_id')
                    ->nullable()
                    ->constrained('status_pembayaran')
                    ->onUpdate('cascade')
                    ->onDelete('cascade');
            $table->foreignId('jenis_pembayaran_id')
                    ->constrained('jenis_pembayaran')
                    ->onUpdate('cascade')
                    ->onDelete('cascade');;
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaksi');
    }
}
